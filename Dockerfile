FROM quay.io/ansible/toolset:latest

# Version env vars
ENV PACKERVERSION=1.7.0

# Install reqs
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y \
    sshpass \
    curl \
    nano \
    unzip \
    jq \
    git \
 && apt-get autoremove --purge \
 && apt-get clean\
 && rm -rf /var/lib/apt/lists/*

# Intall packer binary
RUN curl -fsSLO https://releases.hashicorp.com/packer/${PACKERVERSION}/packer_${PACKERVERSION}_linux_amd64.zip \
 && unzip -p packer_${PACKERVERSION}_linux_amd64.zip packer > /usr/local/bin/packer \
 && chmod 750 /usr/local/bin/packer \
 && rm packer_${PACKERVERSION}_linux_amd64.zip

# crate folder to put auth into.
RUN mkdir /root/.docker

# install working version of DO driver.
RUN pip3 install git+https://github.com/ansible-community/molecule-digitalocean.git

# Add Proxmoxer for proxmox
RUN pip3 install proxmoxer

# store credentials and use those we've stored. 
# This allows for pre-populating the creds allowing noninteractive auth.
RUN git config --global credential.helper store

# Add our env var reading script into container for use by ansivault
COPY ./.ci_* /etc/ansible/

# make an ssh folder for sshing
RUN mkdir /root/.ssh && chmod 700 /root/.ssh

# Configuration env vars

# We tell ansible to read the vault pass from this file 
# beacuse the file should poop it from the env
ENV ANSIBLE_VAULT_PASSWORD_FILE=/etc/ansible/.ci_vault_pass  

#ansible complains if the value is unset or empty
ENV VAULT_PASSWORD="test"

# Don't be too careful about checking host keys
ENV ANSIBLE_HOST_KEY_CHECKING=false

# The ‘smart’ value means each new host that has no facts discovered will be scanned, 
# but if the same host is addressed in multiple plays it will not be contacted again in the playbook run.
ENV ANSIBLE_GATHERING smart 

# Don't create a .retry file because there's no point - new run new container.
ENV ANSIBLE_RETRY_FILES_ENABLED False

# try reduce network load by sending modules all at once, in some cases.
ENV ANSIBLE_SSH_PIPELINING True

# default to looking for the hosts file in the current dir
ENV ANSIBLE_INVENTORY=./hosts.yml
